/*
Написать класс Human, который содержит фамилию, имя и отчество человека
Реализовать 2 конструктора, первый принимает фамилию и имя, второй все 3 параметра
реализовать метод getFullName, который возвращает полное имя (Пупкин Иван Васильевич),
учесть что отчества может не быть (Пупкин Иван)
реализовать метод getShortName который возвращает фамилию и инициалы (Пупкин И. В. или Пупкин И., если отчества нет)
 */
public class Human {
    private String last_name, first_name, patronymic;

    public Human(String last_name, String first_name) {

        this.last_name = last_name;
        this.first_name = first_name;

    }

    public Human(String last_name, String first_name, String patronymic) {

        this(last_name, first_name);
        this.patronymic = patronymic;

    }
    public String getFullName() {

        if (this.patronymic != null) {
            return this.last_name + " " + this.first_name + " " + this.patronymic;
        } else {
            return this.last_name + " " + this.first_name;
        }

    }
    public String getShortName() {

        if (this.patronymic != null) {
            return this.last_name + " " + this.first_name.charAt(0) + ". " + this.patronymic.charAt(0) + ".";
        } else {
            return this.last_name + " " + this.first_name.charAt(0) + ".";
        }

    }

}
