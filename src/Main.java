public class Main {

    public static void main(String[] args) {

        Human human1 = new Human("Пупкин", "Иван", "Васильевич");

        Human human2 = new Human("Пупкин", "Иван");

        System.out.println(human1.getFullName());
        System.out.println(human2.getFullName());

        System.out.println();

        System.out.println(human1.getShortName());
        System.out.println(human2.getShortName());

    }

}
